const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(cors());
app.use(bodyParser.json());

// Importer les routes utilisateur
const userRoutes = require('./routes/userRoutes');

// Utiliser les routes utilisateur
app.use('/api', userRoutes);

// Démarre le serveur sur le port 5000
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Le serveur a démarré sur le port ${PORT}`));
