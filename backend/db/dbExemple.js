const { Pool } = require('pg');

const pool = new Pool({
  user: 'user',
  host: 'host',
  database: 'databaseName',
  password: 'password',
  port: 5432,
});

// Test de connexion
pool.connect((err, client, release) => {
  if (err) {
    console.error('Erreur de connexion à la base de données:', err.stack);
  } else {
    console.log('Connexion à la base de données réussie');
    release();
  }
});


module.exports = pool;