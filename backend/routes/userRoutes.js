const express = require('express');
const router = express.Router();
const pool = require('../db/db');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const SECRET_KEY = 'ton_secret_key'; 

// Route pour l'inscription de l'utilisateur
router.post('/signup', async (req, res) => {
    const { pseudo, motDePasse, mail } = req.body;

    try {
        // Hachage du mot de passe
        const hashedPassword = await bcrypt.hash(motDePasse, 10);

        // Insertion de l'utilisateur dans la base de données
        const result = await pool.query(
            'INSERT INTO "utilisateur" (pseudo, motdepasse, mail) VALUES ($1, $2, $3) RETURNING *',
            [pseudo, hashedPassword, mail]
        );

        res.status(201).json(result.rows[0]);
    } catch (err) {
        console.error(err.message);
        if (err.code === '23505') { // Erreur de contrainte d'unicité (ex : pseudo ou mail déjà pris)
            res.status(400).json({ error: 'Pseudo ou mail déjà utilisé' });
        } else {
            res.status(500).json({ error: 'Erreur serveur' });
        }
    }
});

// Route pour la connexion de l'utilisateur
router.post('/login', async (req, res) => {
    const { pseudo, motDePasse } = req.body;

    try {

        console.log("Début login");


        // Récupération de l'utilisateur depuis la base de données
        const userQuery = await pool.query('SELECT * FROM "utilisateur" WHERE pseudo = $1', [pseudo]);
        const user = userQuery.rows[0];

        // Vérification si l'utilisateur existe
        if (!user) {
            console.log("pseudo : ", user);

            return res.status(400).json({ error: 'Pseudo non existant' });
        }

        // Vérification du mot de passe
        const isPasswordValid = await bcrypt.compare(motDePasse, user.motdepasse);
        if (!isPasswordValid) {
            console.log("Mot de passe : ",  motDePasse);

            return res.status(400).json({ error: 'Mot de passe incorrect' });
        }

        // Création d'un token JWT
        const token = jwt.sign({ userId: user.id, pseudo: user.pseudo }, SECRET_KEY, { expiresIn: '1h' });
        console.log("Token créé : ");

        res.status(200).json({ token });
    } catch (err) {
        console.error('Erreur lors de la connexion:', err);
        res.status(500).json({ error: 'Erreur serveur' });
    }
});

router.post('/add-location', async (req, res) => {
    const { latitude, longitude, nom, idutilisateur } = req.body;
  
    try {
      const result = await pool.query(
        'INSERT INTO pointachatalcool (latitude, longitude, nom, idutilisateur) VALUES ($1, $2, $3, $4) RETURNING *',
        [latitude, longitude, nom, idutilisateur]
      );
      res.status(201).json(result.rows[0]);
    } catch (err) {
      console.error(err);
      res.status(500).send('Erreur lors de l\'ajout de l\'emplacement');
    }
  });


router.get('/get-locations', async (req, res) => { //prends trop de datas d'un coup / Obsolète
try {
    const result = await pool.query(`
        SELECT id, nom, latitude, longitude, encode(image, 'base64') AS image
        FROM pointachatalcool
    `);
    console.log(result.rows);
    res.json(result.rows);
} catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Erreur lors de la récupération des localisations' });
}
});


router.get('/get-basic-locations', async (req, res) => {  //prends les datas intelligemment
    try {
        const result = await pool.query('SELECT id, latitude, longitude FROM pointachatalcool'); 
        res.json(result.rows);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'Erreur lors de la récupération des localisations' });
    }
});


router.get('/get-location-details/:id', async (req, res) => {  //prends les datas intelligemment
    const locationId = req.params.id;

    try {
        const result = await pool.query(
            'SELECT nom, encode(image, \'base64\') AS image FROM pointachatalcool WHERE id = $1',
            [locationId]
        );

        if (result.rows.length === 0) {
            return res.status(404).json({ error: 'Emplacement non trouvé' });
        }

        res.json(result.rows[0]);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'Erreur lors de la récupération des détails de l\'emplacement' });
    }
});



router.post('/add-image', async (req, res) => {
    const { id, idutilisateur, imageBase64 } = req.body;

    if (!imageBase64) {
        return res.status(400).json({ error: 'Aucune image fournie' });
    }

    try {
        // Conversion de l'image base64 en buffer (bytea pour PostgreSQL)
        const imageBuffer = Buffer.from(imageBase64, 'base64');

        // Mise à jour de la table 'pointachatalcool' avec l'image
        const result = await pool.query(
            'UPDATE pointachatalcool SET image = $1, idutilisateur = $2 WHERE id = $3 RETURNING *',
            [imageBuffer, idutilisateur, id]
        );

        if (result.rowCount === 0) {
            return res.status(404).json({ error: 'Point d\'achat d\'alcool non trouvé' });
        }

        res.status(200).json(result.rows[0]);
    } catch (err) {
        console.error('Erreur lors de l\'ajout de l\'image:', err);
        res.status(500).json({ error: 'Erreur serveur' });
    }
});


router.get('/get-horaires/:id', async (req, res) => {
    const idpointachatalcool = parseInt(req.params.id, 10); // Conversion en entier

    if (isNaN(idpointachatalcool)) {
        return res.status(400).json({ error: 'ID invalide' });
    }

    try {
        const result = await pool.query(
            `SELECT 
                lundi_matin, lundi_apres_midi, 
                mardi_matin, mardi_apres_midi, 
                mercredi_matin, mercredi_apres_midi, 
                jeudi_matin, jeudi_apres_midi, 
                vendredi_matin, vendredi_apres_midi, 
                samedi_matin, samedi_apres_midi, 
                dimanche_matin, dimanche_apres_midi  
            FROM horaires 
            WHERE id = $1`,
            [idpointachatalcool]
        );

        if (result.rows.length === 0) {
            return res.status(404).json({ error: 'Point d\'achat d\'alcool non trouvé' });
        }

        // Construire l'objet horaires à partir des résultats de la requête
        const horaires = {
            lundi: {
                matin: result.rows[0].lundi_matin,
                apres_midi: result.rows[0].lundi_apres_midi,
            },
            mardi: {
                matin: result.rows[0].mardi_matin,
                apres_midi: result.rows[0].mardi_apres_midi,
            },
            mercredi: {
                matin: result.rows[0].mercredi_matin,
                apres_midi: result.rows[0].mercredi_apres_midi,
            },
            jeudi: {
                matin: result.rows[0].jeudi_matin,
                apres_midi: result.rows[0].jeudi_apres_midi,
            },
            vendredi: {
                matin: result.rows[0].vendredi_matin,
                apres_midi: result.rows[0].vendredi_apres_midi,
            },
            samedi: {
                matin: result.rows[0].samedi_matin,
                apres_midi: result.rows[0].samedi_apres_midi,
            },
            dimanche: {
                matin: result.rows[0].dimanche_matin,
                apres_midi: result.rows[0].dimanche_apres_midi,
            },
        };

        res.json(horaires); // Renvoyer les horaires au format JSON
    } catch (err) {
        console.error('Erreur lors de la récupération des horaires:', err);
        res.status(500).json({ error: 'Erreur serveur', details: err.message });
    }
});

router.post('/add-horaires', async (req, res) => {
    const { idpointachatalcool, horaires } = req.body;

    if (!idpointachatalcool || !horaires) {
        return res.status(400).json({ error: 'idpointachatalcool et horaires sont requis' });
    }

    try {
        // Convertir les horaires en format JSON
        const horairesJson = JSON.stringify(horaires);

        // Mettre à jour les horaires dans la table
        const result = await pool.query(
            'UPDATE pointachatalcool SET horaires = $1 WHERE id = $2 RETURNING *',
            [horairesJson, idpointachatalcool]
        );

        if (result.rowCount === 0) {
            return res.status(404).json({ error: 'Point d\'achat d\'alcool non trouvé' });
        }

        res.status(200).json(result.rows[0]);
    } catch (err) {
        console.error('Erreur lors de l\'ajout des horaires:', err);
        res.status(500).json({ error: 'Erreur serveur' });
    }
});





module.exports = router;