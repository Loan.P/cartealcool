import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import '../LoginOrSignup.css'; 

const Login = ({ onLogin }) => {
  const [pseudo, setPseudo] = useState('');
  const [motDePasse, setMotDePasse] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    console.log('Connexion Data:', { pseudo, motDePasse });

    try {
      const response = await axios.post('http://localhost:5000/api/login', { pseudo, motDePasse });
      const { token } = response.data;
      
      localStorage.setItem('token', token);

      onLogin(token); 

      navigate('/profil');

    } catch (err) {
      setError('Pseudo ou mot de passe incorrect');
      console.error('Connexion error:', err.response ? err.response.data : err.message);
    }
  };

  return (
    <div className="form-container">
      <form onSubmit={handleSubmit}>
        <h2>Connexion</h2>
        <input 
          type="text" 
          placeholder="Pseudo" 
          value={pseudo} 
          onChange={(e) => setPseudo(e.target.value)} 
          required 
        />
        <input 
          type="password" 
          placeholder="Mot de passe" 
          value={motDePasse} 
          onChange={(e) => setMotDePasse(e.target.value)} 
          required 
        />
        <button type="submit">Se connecter</button>
      </form>
      {error && <p>{error}</p>}
    </div>
  );
};

const Signup = ({ onSignup }) => {
  const [pseudo, setPseudo] = useState('');
  const [motDePasse, setMotDePasse] = useState('');
  const [mail, setMail] = useState('');
  const [message, setMessage] = useState('');

  const handleSignup = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost:5000/api/signup', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ pseudo, motDePasse, mail })
      });

      const data = await response.json();
      if (response.ok) {
        setMessage('Inscription réussie');
        onSignup(pseudo, motDePasse); // Update application state after signup
      } else {
        setMessage(data.error || 'Erreur lors de l\'inscription');
      }
    } catch (error) {
      setMessage('Erreur serveur');
    }
  };

  return (
    <div className="form-container">
      <form onSubmit={handleSignup}>
        <h2>Inscription</h2>
        <input 
          type="text" 
          placeholder="Pseudo" 
          value={pseudo} 
          onChange={(e) => setPseudo(e.target.value)} 
          required 
        />
        <input 
          type="password" 
          placeholder="Mot de passe" 
          value={motDePasse} 
          onChange={(e) => setMotDePasse(e.target.value)} 
          required 
        />
        <input 
          type="email" 
          placeholder="Mail" 
          value={mail} 
          onChange={(e) => setMail(e.target.value)} 
          required 
        />
        <button type="submit">S'inscrire</button>
        {message && <p>{message}</p>}
      </form>
    </div>
  );
};

const LoginOrSignup = ({ onLogin, onSignup }) => {
  return (
    <div className="login-or-signup">
      <h1>Connexion / Inscription</h1>
      <div className="loginSignup">
        <Login onLogin={onLogin} />
        <Signup onSignup={onSignup} />
      </div>
    </div>
  );
};

export default LoginOrSignup;