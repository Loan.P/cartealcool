import React, { useState, useEffect } from 'react';
import { jwtDecode } from "jwt-decode";
import { useNavigate } from 'react-router-dom';

const Profil = () => {
    const navigate = useNavigate();
  
    const token = localStorage.getItem('token');
  
    let user = null;
    if (token) {
      user = jwtDecode(token);
    }
  
    const handleLogout = () => {
      localStorage.removeItem('token');
      navigate('/login');
    };
  
    return (
      <div>
        <h2>Profil Utilisateur</h2>
        {user ? (
          <div>
            <p>Pseudo: {user.pseudo}</p>
            <button onClick={handleLogout}>Se déconnecter</button>
          </div>
        ) : (
          <p>Aucun utilisateur connecté</p>
        )}
      </div>
    );
  };
  
export default Profil;