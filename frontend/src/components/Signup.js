import React, { useState } from 'react';

const Signup = () => {
  const [pseudo, setPseudo] = useState('');
  const [motDePasse, setMotDePasse] = useState('');
  const [mail, setMail] = useState('');
  const [message, setMessage] = useState('');

  const handleSignup = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost:5000/api/signup', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ pseudo, motDePasse, mail })
      });

      const data = await response.json();
      if (response.ok) {
        setMessage('Inscription réussie');
      } else {
        setMessage(data.error || 'Erreur lors de l\'inscription');
      }
    } catch (error) {
      setMessage('Erreur serveur');
    }
  };

  return (
    <form onSubmit={handleSignup}>
      <h2>Inscription</h2>
      <input 
        type="text" 
        placeholder="Pseudo" 
        value={pseudo} 
        onChange={(e) => setPseudo(e.target.value)} 
        required 
      />
      <input 
        type="password" 
        placeholder="Mot de passe" 
        value={motDePasse} 
        onChange={(e) => setMotDePasse(e.target.value)} 
        required 
      />
      <input 
        type="email" 
        placeholder="Mail" 
        value={mail} 
        onChange={(e) => setMail(e.target.value)} 
        required 
      />
      <button type="submit">S'inscrire</button>
      {message && <p>{message}</p>}
    </form>
  );
};

export default Signup;