import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const Login = ({ onLogin }) => {
  const [pseudo, setPseudo] = useState('');
  const [motDePasse, setMotDePasse] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    console.log('Données de connexion:', { pseudo, motDePasse });

    try {
      const response = await axios.post('http://localhost:5000/api/login', { pseudo, motDePasse });
      const { token } = response.data;
      
      localStorage.setItem('token', token);

      onLogin(token); // MAJ état de l'application

      navigate('/profil');


    } catch (err) {
      setError('Pseudo ou mot de passe incorrect');
      console.error('Erreur de connexion:', err.response ? err.response.data : err.message);
    }



  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <h2>Connexion</h2>
        <input 
          type="text" 
          placeholder="Pseudo" 
          value={pseudo} 
          onChange={(e) => setPseudo(e.target.value)} 
          required 
        />
        <input 
          type="password" 
          placeholder="Mot de passe" 
          value={motDePasse} 
          onChange={(e) => setMotDePasse(e.target.value)} 
          required 
        />
        <button type="submit">Se connecter</button>
      </form>
      {error && <p>{error}</p>}
    </div>
  );
};

export default Login;