import React, { useState, useEffect, useRef } from 'react';
import { MapContainer, TileLayer, Marker, useMapEvents, useMap } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import Modal from 'react-modal';

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

Modal.setAppElement('#root');

const MapComponent = ({ userId }) => {
  const [locations, setLocations] = useState([]);
  const [selectedLocation, setSelectedLocation] = useState({ lat: 0, lng: 0 });
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [modalPosition, setModalPosition] = useState({ top: 0, left: 0 });
  const [locationName, setLocationName] = useState('');
  const [selectedMarkerInfo, setSelectedMarkerInfo] = useState(null);
  const [sidebarVisible, setSidebarVisible] = useState(false);
  const [imageBase64, setImageBase64] = useState(null);
  const [modalSuggIsOpen, setModalSuggIsOpen] = useState(false);
  const [suggestedHoraires, setSuggestedHoraires] = useState({});
  const canvasRef = useRef(null);

  const loadBasicLocations = async () => {
    try {
      const response = await fetch('http://localhost:5000/api/get-basic-locations');
      if (response.ok) {
        const data = await response.json();
        setLocations(data);
      } else {
        console.error('Erreur lors de la récupération des localisations');
      }
    } catch (error) {
      console.error('Erreur de connexion à l\'API', error);
    }
  };

  const loadLocationDetails = async (locationId) => {
    setImageBase64(null); // Réinitialiser avant de charger de nouvelles données
    try {
      const response = await fetch(`http://localhost:5000/api/get-location-details/${locationId}`);
      if (response.ok) {
        const data = await response.json();
        setSelectedMarkerInfo({ id: locationId, ...data });
        if (data.image) {
          setImageBase64(data.image);
        }
      } else {
        console.error('Erreur lors de la récupération des détails de l\'emplacement');
      }
    } catch (error) {
      console.error('Erreur de connexion à l\'API', error);
    }
  };

  useEffect(() => {
    loadBasicLocations();
  }, []);

  useEffect(() => {
    if (canvasRef.current) {
      const canvas = canvasRef.current;
      const context = canvas.getContext('2d');

      // Efface le canvas
      context.clearRect(0, 0, canvas.width, canvas.height);

      if (imageBase64) {
        const image = new Image();
        image.onload = () => {
          context.drawImage(image, 0, 0, canvas.width, canvas.height);
        };
        image.src = `data:image/jpeg;base64,${imageBase64}`;
      } else {
        // Afficher un message ou une couleur de fond indiquant qu'aucune image n'est disponible
        context.fillStyle = '#f0f0f0'; // Couleur de fond par défaut
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.fillStyle = '#000';
        context.font = '20px Arial';
        context.textAlign = 'center';
        context.fillText('Aucune image disponible', canvas.width / 2, canvas.height / 2);
      }
    }
  }, [imageBase64]);


  const handleMarkerClick = async (locationId) => {
    loadLocationDetails(locationId);

    // try {
    //     const response = await fetch(`http://localhost:5000/api/get-horaires/${locationId}`);
    //     console.log(response);
    //     console.log(response.status);

    //     if (!response.ok) {
    //         console.error('Erreur lors de la récupération des horaires');
    //         return;
    //     }

    //     const horaires = await response.json();
    //     console.log('Horaires récupérés :', horaires);


    //     setSelectedMarkerInfo(prevInfo => ({
    //         ...prevInfo,
    //         horaires: horaires
    //     }));
    // } catch (error) {
    //     console.error('Erreur de connexion à l\'API pour récupérer les horaires', error);
    // }

    setSidebarVisible(true);
};

  const LocationMarker = () => {
    const map = useMap();

    useMapEvents({
      click(e) {
        setSelectedLocation(e.latlng);
        calculateModalPosition(map, e.latlng);
        setModalIsOpen(true);
        setSidebarVisible(false);
      },
    });

    return selectedLocation.lat !== 0 && selectedLocation.lng !== 0 ? (
      <Marker position={selectedLocation}></Marker>
    ) : null;
  };

  const calculateModalPosition = (map, latlng) => {
    const point = map.latLngToContainerPoint(latlng);
    const modalLeft = point.x + 30;
    const modalTop = point.y - 50;
    setModalPosition({ top: modalTop, left: modalLeft });
  };

  const handleAddLocation = async () => {
    const newLocation = {
      latitude: selectedLocation.lat,
      longitude: selectedLocation.lng,
      nom: locationName,
      idutilisateur: userId,
    };

    try {
      const response = await fetch('http://localhost:5000/api/add-location', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newLocation),
      });

      if (response.ok) {
        setModalIsOpen(false);
        setSelectedLocation({ lat: 0, lng: 0 });
        setLocationName('');
        loadBasicLocations();
      } else {
        console.error('Erreur lors de l\'ajout de l\'emplacement');
        alert('Erreur lors de l\'ajout de l\'emplacement');
      }
    } catch (error) {
      console.error('Erreur de connexion à l\'API', error);
      alert('Erreur de connexion à l\'API');
    }
  };

  const handleCloseModal = () => {
    setModalIsOpen(false);
    setSelectedLocation({ lat: 0, lng: 0 });
    setLocationName('');
    setSelectedMarkerInfo(null);
    setSidebarVisible(false);
  };

  const handleCanvasClick = () => {
    if (!imageBase64) {
      const input = document.createElement('input');
      input.type = 'file';
      input.accept = 'image/*';
      input.onchange = async (e) => {
        const file = e.target.files[0];
        if (file) {
          const reader = new FileReader();
          reader.onloadend = async () => {
            const base64String = reader.result.split(',')[1];
            setImageBase64(base64String);
            await uploadImage(base64String);
          };
          reader.readAsDataURL(file);
        }
      };
      input.click();
    }
  };

  const uploadImage = async (base64Image) => {
    try {
      const response = await fetch('http://localhost:5000/api/add-image', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id: selectedMarkerInfo.id,
          idutilisateur: userId,
          imageBase64: base64Image,
        }),
      });

      if (response.ok) {
        console.log('Image ajoutée avec succès');
      } else {
        console.error('Erreur lors de l\'ajout de l\'image');
        alert('Erreur lors de l\'ajout de l\'image');
      }
    } catch (error) {
      console.error('Erreur de connexion à l\'API', error);
      alert('Erreur de connexion à l\'API');
    }
  };

  const handleSuggestHoraires = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://localhost:5000/api/add-horaires', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          idpointachatalcool: selectedMarkerInfo.id,
          horaires: suggestedHoraires
        }),
      });

      if (response.ok) {
        console.log('Horaires suggérés ajoutés avec succès');
        setModalSuggIsOpen(false);
        setSuggestedHoraires({});
        // Rafraîchir les horaires après ajout
        handleMarkerClick(selectedMarkerInfo.id);
      } else {
        console.error('Erreur lors de l\'ajout des horaires suggérés');
        alert('Erreur lors de l\'ajout des horaires suggérés');
      }
    } catch (error) {
      console.error('Erreur de connexion à l\'API pour suggérer des horaires', error);
      alert('Erreur de connexion à l\'API');
    }
  };

  const closeSidebar = () => {
    setSidebarVisible(false);
  };

  return (
    <div className="map-container">
      <MapContainer
        center={[48.4011383, -4.3945645]}
        zoom={15}
        style={{ height: "100vh", width: "100%" }}
        maxZoom={22}
        minZoom={1}
      >
        <TileLayer
          url="https://api.maptiler.com/maps/basic-v2/{z}/{x}/{y}.png?key=fauGb28HKHgrVtxliY8b"
          attribution="Map data &copy; <a href='https://www.maptiler.com/'>MapTiler</a>"
          maxZoom={22}
        />
        {locations.map((location) => (
          <Marker
            key={location.id}
            position={[parseFloat(location.latitude), parseFloat(location.longitude)]}
            eventHandlers={{
              click: () => {
                handleMarkerClick(location.id);
              }
            }}
          >
            {/* <Popup>Chargement...</Popup> */}
          </Marker>
        ))}
        <LocationMarker />
      </MapContainer>

      {modalIsOpen && (
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={handleCloseModal}
          contentLabel="Ajouter un emplacement"
          style={{
            overlay: { zIndex: 1000 },
            content: {
              top: `${modalPosition.top}px`,
              left: `${modalPosition.left + 280}px`,
              right: 'auto',
              bottom: 'auto',
              marginRight: '-50%',
              transform: 'translate(-50%, -50%)'
            }
          }}
        >
          <h2 className="TitreAjoutEmplacement">Ajouter un emplacement</h2>
          <input
            className="InputAjoutEmplacement"
            type="text"
            value={locationName}
            onChange={(e) => setLocationName(e.target.value)}
            placeholder="Nom de l'emplacement"
          />
          <div>
            <button className="boutonAjoutEmplacement" onClick={handleAddLocation}>Ajouter cet emplacement</button>
            <button className="boutonAjoutEmplacement" onClick={handleCloseModal}>Fermer</button>
          </div>
        </Modal>
      )}

      {modalSuggIsOpen && (
        <Modal
          isOpen={modalSuggIsOpen}
          onRequestClose={() => setModalSuggIsOpen(false)}
          contentLabel="Suggérer des horaires"
          style={{
            overlay: { zIndex: 1000 },
            content: {
              top: '50%',
              left: '50%',
              right: 'auto',
              bottom: 'auto',
              marginRight: '-50%',
              transform: 'translate(-50%, -50%)',
            }
          }}
        >
          <h2>Suggérer des horaires</h2>
          <form onSubmit={handleSuggestHoraires}>
            {['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'].map(day => (
              <div key={day}>
                <label>{day.charAt(0).toUpperCase() + day.slice(1)}</label>
                <input
                  type="text"
                  placeholder="Horaires (ex: 09:00-12:00)"
                  onChange={(e) => setSuggestedHoraires({ ...suggestedHoraires, [day]: e.target.value })}
                />
              </div>
            ))}
            <button type="submit">Enregistrer</button>
            <button type="button" onClick={() => setModalSuggIsOpen(false)}>Annuler</button>
          </form>
        </Modal>
      )}

      <div className={`sidebar ${sidebarVisible ? 'visible' : ''}`}>
        {selectedMarkerInfo && (
          <div className="marker-details">
            <button className="close-sidebar" onClick={closeSidebar}>×</button>
            <h2>{selectedMarkerInfo.nom || 'Nom en cours de chargement...'}</h2>
            <canvas 
              ref={canvasRef} 
              width="600" 
              height="200" 
              style={{ border: '1px solid #ccc', cursor: 'pointer' }}
              onClick={handleCanvasClick}
            ></canvas>
            <div className="CompartimentInformationIcon">
              <div className="Informations">
                <img src='../img/LogoHorloge.png' alt='Logo horaire' className="IconInformation" />
                <p>{selectedMarkerInfo.horaires ? selectedMarkerInfo.horaires : 'Horaires non disponibles'}</p>



                <button className="boutonSuggestionHoraires" onClick={() => setModalSuggIsOpen(true)}>Suggérer des horaires</button>
              </div>
              <div className="Informations">
                <img src='../img/LogoLocalisation.png' alt='Logo localisation' className="IconInformation" />
                <p>10 rue de la Grande Lande</p>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default MapComponent;

