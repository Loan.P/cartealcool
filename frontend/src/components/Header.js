import React from 'react';
import { Link } from 'react-router-dom';

const Header = ({ isLoggedIn }) => {
  return (
    <nav>
      <Link to="/mapcomponent">Carte</Link>
      <Link to="/add-location">Ajouter un lieu</Link>
      <Link to="/login">Connexion</Link>
      <Link to="/signup">Inscription</Link>
      {isLoggedIn && <Link to="/profil">Profil</Link>}
    </nav>
  );
};

export default Header;