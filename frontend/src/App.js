import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import MapComponent from './components/MapComponent';
import LoginOrSignup from './components/LoginOrSignup';
import Profil from './components/Profil';
import './App.css';

const App = () => {
  const [locations, setLocations] = useState([]);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  // Récupération des localisations point d'alcool
  const fetchLocations = async () => {
    try {
      const response = await fetch('http://localhost:5000/api/get-locations');
      if (response.ok) {
        const data = await response.json();
        setLocations(data);
      } else {
        console.error('Erreur lors de la récupération des emplacements');
      }
    } catch (error) {
      console.error('Erreur de connexion à l\'API', error);
    }
  };

  useEffect(() => {
    fetchLocations();
  }, []);

  const handleLogin = (token) => {
    setIsLoggedIn(true);
    // Vous pouvez éventuellement gérer l'état de connexion ici
  };

  const handleSignup = (pseudo, motDePasse) => {
    setIsLoggedIn(true);
    // Vous pouvez éventuellement gérer l'état d'inscription ici
  };

  return (
    <Router>
      <div className="App">
        {/* Header superposé */}
        <header className="App-header">
          <div className="buttons-container">
          <a href='loginorsignup'>
            <button className="round-button" onClick={() => console.log('Bouton Connexion/Inscription')}>
              <img src='./img/logobonhome.png' alt='Logo profil utilisateur' class="boutonProfil"></img>
            </button>
            </a>
            {isLoggedIn && (
              <button className="round-button" onClick={() => console.log('Bouton Profil')}>
                Profil
              </button>
            )}
          </div>
        </header>

        {/* Contenu principal */}
        <div className="content">
          <Routes>
            <Route path="/" element={<Navigate to="/mapcomponent" />} />
            <Route path="/mapcomponent" element={<MapComponent locations={locations} />} />
            <Route path="/loginorsignup" element={<LoginOrSignup onLogin={handleLogin} onSignup={handleSignup} />} />
            <Route path="/profil" element={isLoggedIn ? <Profil /> : <Navigate to="/login" />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
};

export default App;